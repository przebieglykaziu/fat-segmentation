import dicom
import os
import numpy as np
from functools import partial
from joblib import Parallel, delayed

def load_dicom(dcm_dir_path):
    fnames = list(os.scandir(dcm_dir_path))
    slices = [dicom.read_file(f.path) for f in fnames]
    slope = slices[0].RescaleSlope
    intercept = slices[0].RescaleIntercept
    img = np.stack((s.pixel_array * slope + intercept for s in slices), axis=0)
    return img, fnames

def apply_to_volume(f, v, parallel=True, n_jobs=-1, **kwargs):
    if parallel:
        task = partial(f, **kwargs)
        return np.array(Parallel(n_jobs=n_jobs, backend='threading')(
            delayed(task)(i) for i in v))
    return np.stack((f(i, **kwargs) for i in v), axis=0)
