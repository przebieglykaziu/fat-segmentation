import os
import numpy as np
import argparse
#  from tqdm import tqdm
from skimage.morphology import binary_closing, binary_opening, convex_hull_image, disk
from skimage.segmentation import random_walker
from skimage.exposure import rescale_intensity, equalize_adapthist
from skimage.io import imsave
from scipy.ndimage import binary_fill_holes
from util import load_dicom, apply_to_volume

tissue_threshold = -274 # 750 - 1024
not_fat_threshold = -24 # 1000 - 1024 
fat_upper_threshold = -74 # 950 - 1024
fat_lower_threshold = -214 # 810 - 1024
muscle_threshold = 26 # 1050 - 1024 
background = -1024

def get_body_mask(img):
    s = disk(5)
    tissue = img > tissue_threshold  # all voxels in the image that are not air
    closed_and_filled = apply_to_volume(close_and_fill_holes, tissue, selem=s)
    body_mask = apply_to_volume(binary_opening, closed_and_filled, selem=s)
    return body_mask

def get_abdomen_mask(img):
    not_fat = img > not_fat_threshold  # everything that's not fat
    abdo = apply_to_volume(binary_opening, not_fat, selem=disk(4))
    abdo_mask = apply_to_volume(convex_hull_image, abdo)
    return abdo_mask

def label_regions(img, abdo_mask):
    fat_all = (img > fat_lower_threshold) & (
        img < fat_upper_threshold)  # subcutaneous + visceral fat
    visceral_fat = fat_all & abdo_mask
    visceral_fat = apply_to_volume(open_and_close, visceral_fat, selem=disk(1))
    vmask = apply_to_volume(convex_hull_image, visceral_fat)
    sub_fat = fat_all & ~abdo_mask
    #  sub_fat = binary_closing(binary_opening(sub_fat, selem=disk(1)), selem=disk(1))
    muscle = (img >= not_fat_threshold) & (
        img < muscle_threshold)
    muscle = apply_to_volume(close_and_fill_holes, muscle, selem=disk(4))
    muscle = muscle & abdo_mask & ~vmask
    #  bone = img >= 250
    labels = np.zeros(img.shape, dtype=np.uint8)
    #  labels[visceral_fat] = 1
    #  labels[sub_fat] = 2
    #  labels[muscle] = 3
    #  labels[bone] = 4
    #  labels[~abdo_mask] = -1
    labels = np.where(visceral_fat, 1, labels)
    labels = np.where(sub_fat, 2, labels)
    labels = np.where(muscle, 3, labels)
    labels = np.where(img == background, -1, labels)
    return labels

def random_walker_label(img, labels, beta=130):
    img_rescaled = rescale_intensity(img, out_range=(0, img.max() + np.abs(img.min()))).astype(np.uint8)
    img_equalized = np.stack((equalize_adapthist(i, nbins=i.max(), kernel_size=8) for i in img_rescaled), axis=0)
    seg = np.stack((random_walker(s, beta=beta, labels=labels[i]) for i, s in enumerate(img_equalized)), axis=0)
    seg[img > 70] = 0
    muscle = seg == 3
    muscle_final = apply_to_volume(close_and_fill_holes, muscle, selem=disk(1))
    visceral = labels == 1
    seg[seg == 1] = 0
    seg = np.where(visceral, 1, seg)
    seg = np.where(muscle_final, 3, seg)
    return seg

def label_volume(img, use_random_walker=True):
    body_mask = get_body_mask(img)
    body = np.where(body_mask, img, background)
    abdo_mask = get_abdomen_mask(body)
    label_map = label_regions(body, abdo_mask)
    if use_random_walker:
        label_map = random_walker_label(body, label_map)
    return label_map

def close_and_fill_holes(mask, selem):
    return binary_fill_holes(binary_closing(mask, selem=selem), structure=selem)

def open_and_close(mask, selem):
    return binary_closing(binary_opening(mask, selem=selem), selem=selem)

def labels2ppm(labels):
    labels[labels == 1] += 5
    return 10 ** labels

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--input_dir', required=True,
                        help='path to folder containing DICOM images')
    parser.add_argument('--output_dir', required=True,
                        help='path to folder where the generated label maps will be saved')
    parser.add_argument('--export_format', required=False, choices=['ppm', 'numpy'], default='numpy',
                        help='whether to export the labels as Numpy arrays (default) or PPM files.')
    parser.add_argument('--n_jobs', required=False, type=int, default=-1,
                        help='number of threads to use for image processing (maximum available by default)')
    a = parser.parse_args()

    for patient_dir in os.scandir(a.input_dir):
        img, fnames = load_dicom(patient_dir)
    #  label_maps = Parallel(n_jobs=a.n_jobs, backend='threading')(
           #  delayed(label_volume)(i) for i in img)
        label_maps = label_volume(img)

        out_dir = os.path.join(a.output_dir, patient_dir.name) 
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        for s, f in zip(label_maps, fnames):
            if a.export_format == 'numpy':
                np.save(os.path.join(out_dir, os.path.splitext(f.name)[0] + '_labels.npy'), s)
            else:
                label_map = labels2ppm(s)
                imsave(os.path.join(out_dir, os.path.splitext(f.name)[0] + '_labels.ppm'), label_map)


if __name__ == '__main__':
    main()
